package com.example.magida.sifandroidcourse

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ArrayAdapter
import com.example.magida.sifandroidcourse.ressources.Course
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_logged_in.*

class LoggedInActivity : AppCompatActivity() {

    var fbAuth = FirebaseAuth.getInstance()
    val fbDBReference = FirebaseDatabase.getInstance().reference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logged_in)

        btnLogout.setOnClickListener { view ->
            showMessage(view, "Logging Out...")
            signOut()
        }

        fbAuth.addAuthStateListener {
            if (fbAuth.currentUser == null) {
                this.finish()
            }
        }

        loadCourses()

    }

    private fun loadCourses() {
        val courses = mutableListOf<Course>()
        val courseListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                dataSnapshot.children.mapNotNullTo(courses) { it.getValue<Course>(Course::class.java) }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                println("loadPost:onCancelled ${databaseError.toException()}")
            }
        }
        fbDBReference.child("courses").addListenerForSingleValueEvent(courseListener)

        courses.forEach { println("name: ${it.name} - UUID: ${it.key}") }

        val listView = listSubjects

        val data = courses.map { it.name }

        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data)

        listView.adapter = adapter

    }

    private fun signOut() {
        fbAuth.signOut()
    }

    private fun showMessage(view: View, message: String) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).setAction("Action", null).show()
    }
}
