package com.example.magida.sifandroidcourse

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_foo_activity.*

class fooActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_foo_activity)

        val message = intent.getStringExtra("click me!")

        textView.text = message
    }


}
