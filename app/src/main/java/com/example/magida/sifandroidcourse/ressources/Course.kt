package com.example.magida.sifandroidcourse.ressources

/**
 * Created by Magida on 18-03-2018.
 */
data class Course(val name: String = "", var key: String = "")