package com.example.magida.sifandroidcourse

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.rxkotlin.toObservable
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val dataSet = arrayListOf("This", "is", "an", "example", "of", "RX!")
    var fbAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLogin.setOnClickListener {
            signIn(it, inputEmail.text.toString(), inputPassword.text.toString())
        }

        btnSignup.setOnClickListener {
            createUser(it, inputEmail.text.toString(), inputPassword.text.toString())
        }
    }

    private fun signIn(view: View, email: String, password: String) {
        showMessage(view, "Authenticating...")
        fbAuth
                .signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, {
                    if (it.isSuccessful) {
                        val intent = Intent(this, LoggedInActivity::class.java)
                        intent.putExtra("id", fbAuth.currentUser?.email)
                        startActivity(intent)
                    } else {
                        showMessage(view, "Error: ${it.exception?.message}")
                    }
                })

    }

    private fun createUser(view: View, email: String, password: String) {
        showMessage(view, "Creating user...")
        fbAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, {
                    if (it.isSuccessful) {
                        showMessage(view, "User $email was successfully created")
                        signIn(view, email, password)
                    } else {
                        showMessage(view, "Error: ${it.exception?.message}")
                    }
                })


    }

    private fun showMessage(view: View, message: String) {

        Snackbar.make(view, message, Snackbar.LENGTH_LONG).setAction("Action", null).show()
    }

    fun changeActivity(view: View) {
        val intent = Intent(this, fooActivity::class.java).apply { }
        startActivity(intent)
    }

    fun showList(view: View) {
        val lookUp = mapOf(
                1 to "one",
                2 to "two",
                3 to "three",
                4 to "four",
                5 to "five")
        getObservable()
                .filter { it >= 3 }
                .map { lookUp[it] as String }
                .subscribeBy(
                        onNext = {
                            Toast.makeText(applicationContext, "it = $it", Toast.LENGTH_SHORT).show()
                        },
                        onError = { it.printStackTrace() },
                        onComplete = {
                            runOnUiThread {
                                Toast.makeText(applicationContext, "Done printing numbers", Toast.LENGTH_SHORT).show()
                            }
                        }
                )
    }

    fun getObservable(): Observable<Int> {
        val list = listOf(1, 2, 3, 4, 5)
        return list.toObservable()
    }
}
